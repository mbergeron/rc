(require 'flycheck)

;;; Use node_modules eslint if available
(defun use-eslint-from-node-modules ()
  (let* ((root (locate-dominating-file
		(or (buffer-file-name) default-directory) "node_modules"))
	 (eslint (and root
		      (expand-file-name "node_modules/eslint/bin/eslint.js"
					root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))


(defun enable-minor-mode (my-pair)
  "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
  (if (buffer-file-name)
      (if (string-match (car my-pair) buffer-file-name)
	  (funcall (cdr my-pair)))))

;; flycheck configuration
(add-hook 'flycheck-mode-hook 'add-node-modules-path)
(flycheck-add-mode 'javascript-eslint 'web-mode)

;; register web-mode
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[m]?js[x]?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ts[x]?\\'" . web-mode))
;; Disable web-mode for sass, use `sass-mode` instead
(add-to-list 'auto-mode-alist '("\\.s?[ca]ss\\'" . sass-mode))

(defun rainbow-mode-setup ()
	(add-to-list 'rainbow-html-colors-major-mode-list 'json-mode))

(add-hook 'rainbow-mode-hook 'rainbow-mode-setup)

;; web-mode configuration
(defun web-mode-setup ()
  (interactive)
  "Hooks for Web mode."
  (setq indent-tabs-mode nil)
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-script-padding 0)
	(setq web-mode-enable-comment-annotation t)
	(setq web-mode-content-types-alist
				'(("jsx" . "\\.[c|m]?js[x]?\\'"))))

(add-hook 'web-mode-hook 'web-mode-setup)
(add-hook 'web-mode-hook 'add-node-modules-path)
(add-hook 'web-mode-hook 'rainbow-mode)


(defun json-mode-setup ()
	(interactive)
	"Hooks for JSON mode."
	(setq intent-tabs-mode nil)
	(setq js-indent-level 2))

(add-hook 'json-mode-hook 'json-mode-setup)
(add-hook 'json-mode-hook 'rainbow-mode)
