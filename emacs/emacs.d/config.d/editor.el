;; General editor style

(defun my-prog-mode-hook ()
	"My `prog-mode` hook."
	(interactive)
	(linum-mode)
	;; (flycheck-mode)
	;; (highlight-indent-guides-mode)
	(yafolding-mode)
	(yas-minor-mode)
	(emojify-mode)
	;; (undo-tree-mode)
	;; (company-mode)
	;;(setq flycheck-check-syntax-automatically '(mode-enabled save))
	(setq undo-tree-auto-save-history nil)
	(setq yas-ident-line 'auto)
	(setq tab-width 2)
	;; (setq highlight-indent-guides-method 'character)
	;; (setq highlight-indent-guides-character ?\¦)
	(setq mouse-wheel-progressive-speed nil)
	(setq helm-ag-use-agignore t)
	;; Disable backups
	(setq make-backup-files nil)
	)

(add-hook 'prog-mode-hook 'my-prog-mode-hook)

(defun align-colons (beg end)
	(interactive "r")
	(align-regexp beg end ":\\(\\s-*\\)" 1 1 t))

(defun align-hash (beg end)
	(interactive "r")
	(align-regexp beg end "\\(\\s-*\\)\=\>\\(\\s-*\\)" 1 1 t))

(defun align-hash (beg end)
	(interactive "r")
	(align-regexp beg end "\\(\\s-*\\)\=\>\\(\\s-*\\)" 1 1 t))

(defun align-commas (beg end)
	(interactive "r")
	(align-regexp beg end ",\\(\\s-*\\)" 1 1 t))

;; disable global-eldoc-mode
(global-eldoc-mode -1)

;; set fonts
;;(set-frame-font "-FBI -Input Mono Condensed-normal-normal-semicondensed-*-16-*-*-*-m-0-iso10646-1" nil t)
;;(set-frame-font "-JENS-Sudo-normal-normal-normal-*-18-*-*-*-m-0-iso10646-1" nil t)

(setq cursor-type 'bar)

;; icons
(use-package all-the-icons)
;;(require 'treemacs-all-the-icons)

;; modeline
(use-package doom-modeline
	:hook (after-init . doom-modeline-mode))
;;(require 'doom-modeline)
;;(add-hook 'after-init #'doom-modeline-mode)
(doom-modeline-mode t)

(use-package doom-themes
	:config
	;; Global settings (defaults)
	(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
				doom-themes-enable-italic t) ; if nil, italics is universally disabled
	(load-theme 'doom-one t)

	;; Enable flashing mode-line on errors
	;;(doom-themes-visual-bell-config)
	
	;; Enable custom neotree theme (all-the-icons must be installed!)
	;;(doom-themes-neotree-config)
	;; or for treemacs users
	;;(setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
	(doom-themes-treemacs-config)
	
	;; Corrects (and improves) org-mode's native fontification.
	(doom-themes-org-config)
	)

;; A simple config:
;;(use-package solaire-mode
;;  :hook (after-init . solaire-global-mode))

;; treemacs
(use-package treemacs
	:demand t)

(use-package treemacs-evil
	:after treemacs evil
	:ensure t)

(use-package treemacs-icons-dired
	:after treemacs dired
	:ensure t
	:config (treemacs-icons-dired-mode))

(use-package treemacs-magit
	:after treemacs magit
	:ensure t)

(add-hook 'after-init (lambda()
												(global-visual-line-mode 1)
												(menu-bar-mode -1)
												(toggle-scroll-bar -1)
												(tool-bar-mode -1)
												))
