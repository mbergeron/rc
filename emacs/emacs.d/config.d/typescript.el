(defun tide-setup-hook ()
	(interactive)
	(lsp)
	;;(tide-setup)
	(eldoc-mode)
	;;(run-import-js)
	;;(tide-hl-identifier-mode +1)
	;; Cannot add 'lsp-organize-imports as it is async 
	;;(add-hook 'before-save-hook #'lsp-organize-imports)
	(setq web-mode-enable-auto-quoting nil)
	(setq web-mode-markup-indent-offset 2)
	(setq web-mode-code-indent-offset 2)
	(setq web-mode-attr-indent-offset 2)
	(setq web-mode-attr-value-indent-offset 2)
	(setq flycheck-local-checkers '((lsp . ((next-checkers . ('javascript-eslint))))))
  (setq company-dabbrev-downcase nil)
	(setq lst-ui-sideline-show-code-actions t)
 	;;(setq lsp-eslint-server-command '("node" (concat (getenv "HOME") "/var/src/vscode-eslint/server/out/eslintServer.js") "--stdio"))
	(set (make-local-variable 'company-backends)
			 '((company-tide company-files :with company-yasnippet)
				 (company-dabbrev-code company-dabbrev))))

;; format on save
;;(add-hook 'before-save-hook 'tide-format-before-save)

;; use rjsx-mode for .js* files except json and use tide with rjsx
(add-to-list 'auto-mode-alist '("\\.js.*$" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . json-mode))
(add-hook 'rjsx-mode-hook 'tide-setup-hook)

;; web-mode extra config
(add-hook 'web-mode-hook 'tide-setup-hook
					(lambda () (pcase (file-name-extension buffer-file-name)
											 ("tsx" ('tide-setup-hook)))))

;; flycheck
;;(flycheck-add-mode 'typescript-tslint 'web-mode)
;;(flycheck-add-next-checker 'lsp 'typescript-tslint)
(add-hook 'web-mode-hook 'company-mode)
(add-hook 'web-mode-hook 'prettier-js-mode)
;;(add-to-list 'flycheck-disabled-checkers 'tsx-tide)
;;(add-to-list 'flycheck-disabled-checkers 'jsx-tide)
;;(add-to-list 'flycheck-disabled-checkers 'typescript-tslint)
;;(add-hook 'web-mode-hook #'turn-on-smartparens-mode t)
