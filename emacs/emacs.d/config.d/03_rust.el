;; Set path to racer binary
(setq racer-cmd "/usr/bin/racer")

;; Set path to rust src directory
(setq racer-rust-src-path "/usr/src/rust/src")

;; Load rust-mode when you open `.rs` files
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

;; Setting up configurations when you load rust-mode
(add-hook 'rust-mode-hook
	  '(lambda ()
	     ;; Enable racer
	     (racer-mode)
	     (eldoc-mode)
	     ;; Reduce the time after which the company auto completion popup opens
	     (setq company-idle-delay 0.1)
	     ;; Reduce the number of characters before company kicks in
	     (setq company-minimum-prefix-length 0)
	     (setq company-tooltip-align-annotations t)
	     ;; Use flycheck-rust in rust-mode
	     (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
	     ;; Key binding to jump to method definition
	     (local-set-key (kbd "M-é") #'racer-find-definition)
	     ;; Key binding to auto complete and indent
	     (local-set-key (kbd "TAB") #'company-indent-or-complete-common)))

(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'racer-mode-hook #'company-mode)

(setq company-tooltip-align-annotations t)
