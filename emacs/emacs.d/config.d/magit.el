(require 'magit)

;; Do not show diffs on commit
(remove-hook 'server-switch-hook 'magit-commit-diff)

;; Remove some sections in the diff-view for faster
;; statuses
(remove-hook 'magit-status-sections-hook 'magit-insert-stashes)
(remove-hook 'magit-status-sections-hook 'magit-insert-unpushed-to-upstream-or-recent)
(remove-hook 'magit-status-sections-hook 'magit-insert-unpushed-to-pushremote)
(remove-hook 'magit-status-sections-hook 'magit-insert-unpulled-from-pushremote)
(remove-hook 'magit-status-sections-hook 'magit-insert-unpulled-from-upstream)
