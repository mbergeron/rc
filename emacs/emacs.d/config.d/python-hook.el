(defun projectile-find-virtualenv()
  "searches for a .venv directory in the project's root"
  (let (root venv_path)
    (setq root (file-name-as-directory (projectile-project-root)))
    (setq venv_path (concat root ".venv"))
    (if (file-exists-p venv_path) venv_path)))

(add-hook 'python-mode-hook
	  (lambda ()
	    (setq-default indent-tabs-mode nil)
	    (setq-default python-indent-offset 4)
	    (setq-default python-indent-guess-indent-offset nil)
            ;; (let (venv (projectile-find-virtualenv))
            ;;   (if venv
            ;;       (setq jedi:server-args
            ;;             '("--virtual-env" venv)
            ;;       )))
            ;;(jedi:setup)
            (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
