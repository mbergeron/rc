(defun org-random-cmp (a b)
  "Return -1,0 or 1 randomly"
  (- (mod (random) 3) 1))

(defun org-mode-setup ()
	"Org-mode hook"
	(interactive)
	(emojify-mode)
	(add-to-list 'org-agenda-custom-commands
							 '("r" "All todo items in a random order"
								 alltodo ""
								 ((org-agenda-cmp-user-defined #'org-random-cmp))))
	(setq org-tags-column -120)
	(setq org-cycle-separator-lines -1))


(defun org-agenda-mode-setup ()
	"Org-agenda hook"
	(interactive)
	(emojify-mode))


(add-hook 'org-mode-hook 'org-mode-setup)
(add-hook 'org-agenda-mode-hook 'org-agenda-mode-setup)
