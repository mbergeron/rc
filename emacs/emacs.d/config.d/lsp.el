(setq gc-cons-threshold (* 1024 1024 100))     ;; 100mb
(setq read-process-output-max (* 1024 1024 4)) ;; 4mb

;; this enabled per-mode flycheck checkers for lsp
(defvar-local flycheck-local-checkers nil)
(defun +flycheck-checker-get(fn checker property)
	(or (alist-get property (alist-get checker flycheck-local-checkers))
			(funcall fn checker property)))
		(advice-add 'flycheck-checker-get :around '+flycheck-checker-get)
