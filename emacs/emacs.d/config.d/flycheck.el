(require 'flycheck)

(setq-default flycheck-check-syntax-automatically '(mode-enabled save))
