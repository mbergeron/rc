# change the prefix from 'C-b' to 'C-a'
# (remap capslock to CTRL for easy access)
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# start with window 1 (instead of 0)
set -g base-index 1

# start with pane 1
set -g pane-base-index 1

# split panes using | and -
bind v split-window -h
bind h split-window -v
unbind '"'
unbind %

# reload config file
bind r source-file ~/.tmux.conf

unbind p
bind p previous-window

# shorten command delay
set -sg escape-time 1

# don't rename windows automatically
set-option -g allow-rename off

# mouse control (clickable windows, panes, resizable panes)
set -g mouse on

# Use Alt-arrow keys without prefix key to switch panes
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# enable vi mode keys
set-window-option -g mode-keys vi
#bind -t vi-copy 'v' begin-selection
#bind -t vi-copy 'y' copy-selection

# set default terminal mode to 256 colors
set -g default-terminal "screen-255color"

# present a menu of URLs to open from the visible pane. sweet.
bind-key u capture-pane \;\
    save-buffer /tmp/tmux-buffer \;\
    split-window -l 10 "urlview /tmp/tmux-buffer"

# copy to clipboard
bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xclip -i -f -selection clipboard"

######################
### DESIGN CHANGES ###
######################

# panes
set -g pane-border-style fg=black
set -g pane-active-border-style fg=brightred

## Status bar design
# status line
set -g status-style fg=colour12,bg=default
set -g status-justify left
set -g status-interval 2
set -g status-left '#S —'
set -g status-right '%H:%M'

# messaging
set -g message-style fg=black,bg=yellow
set -g message-command-style fg=blue,bg=black

# window status
setw -g window-status-format " #I:#W "
setw -g window-status-current-format " #F#I:#W "
#setw -g window-status-format "#[fg=magenta]#[bg=black] #I #[bg=cyan]#[fg=colour8] #W "
#setw -g window-status-current-format "#[bg=brightmagenta]#[fg=colour8] #I #[fg=colour8]#[bg=colour14] #W "
setw -g window-status-current-style fg=colour11
#setw -g window-status-current-attr dim
setw -g window-status-style fg=colour8,bg=black
#setw -g window-status-attr reverse

# loud or quiet?
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

set -g default-terminal "screen-256color"
